//
//  main.m
//  TumblrBrowser
//
//  Created by Mariusz Graczkowski on 04.04.2016.
//  Copyright © 2016 Mariusz Graczkowski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
